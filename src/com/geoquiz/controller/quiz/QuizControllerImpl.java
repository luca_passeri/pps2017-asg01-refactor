package com.geoquiz.controller.quiz;

import java.io.InputStream;
import java.util.Optional;
import java.util.Set;

import javax.xml.bind.JAXBException;

import com.geoquiz.model.quiz.*;
import com.geoquiz.utility.ResourceLoader;
import com.geoquiz.view.utility.Category;
import com.geoquiz.view.utility.Difficulty;
import com.geoquiz.view.utility.Modality;
import com.sun.org.apache.xpath.internal.operations.Mod;

/**
 * Implementation of QuizController interface.
 *
 */
public class QuizControllerImpl implements QuizController {

    private static final double FREEZE_TIME = 3000;

    private Quiz quiz;

    /**
     * @param category
     *            the category of the quiz.
     * @param modality
     *            the modality of the quiz.
     * @throws JAXBException
     *             exception.
     */
    public QuizControllerImpl(final Category category, final Mode modality)
            throws JAXBException {

        switch (category) {

        case CAPITALI:
            this.quiz = QuizFactory.createCapitalsQuiz(modality);
            break;

        case MONUMENTI:
            this.quiz = QuizFactory.createMonumentsQuiz(modality);
            break;

        case VALUTE:
            this.quiz = QuizFactory.createCurrenciesQuiz(Optional.of(modality));
            break;

        case BANDIERE:
            this.quiz = QuizFactory.createFlagsQuiz(Optional.of(modality));
            break;

        case CUCINA:
            this.quiz = QuizFactory.createTypicalDishesQuiz(Optional.of(modality));
            break;

        default:
            throw new IllegalArgumentException();

        }
    }

    @Override
    public String showStringQuestion() {
        return this.quiz.getCurrentQuestion().getQuestion();
    }

    @Override
    public InputStream showImageQuestion() {

        return ResourceLoader.loadResourceAsStream("/images/flags/" + this.quiz.getCurrentQuestion().getQuestion());

    }

    @Override
    public void hitAnswer(final Optional<String> answer) {
        this.quiz.hitAnswer(answer);

    }

    @Override
    public void nextQuestion() {
        if (!this.gameOver()) {
            this.quiz.next();
        }

    }

    @Override
    public boolean checkAnswer() {
        return this.quiz.isAnswerCorrect();
    }

    @Override
    public String getCorrectAnswer() {
        return this.quiz.getCorrectAnswer();
    }

    @Override
    public int getRemainingLives() {
        return this.quiz.getRemainingLives();
    }

    @Override
    public boolean gameOver() {
        return this.quiz.gameOver();
    }

    @Override
    public boolean isFreezeAvailable() {
        return this.quiz.isFreezeAvailable();
    }

    @Override
    public boolean isSkipAvailable() {
        return this.quiz.isSkipAvailable();
    }

    @Override
    public boolean is5050Available() {
        return this.quiz.is5050Available();
    }

    @Override
    public double freeze() {
        this.quiz.freeze();
        return QuizControllerImpl.FREEZE_TIME;
    }

    @Override
    public void skip() {
        this.quiz.skip();
    }

    @Override
    public Set<String> use5050() {
        return this.quiz.use5050();
    }

    @Override
    public int getQuestionDuration() {
        return this.quiz.getQuestionDuration();
    }

    @Override
    public void restart() {
        this.quiz.restart();
    }

    @Override
    public Set<String> showAnswers() {
        return this.quiz.getCurrentQuestion().getAnswers();
    }

    @Override
    public int getScore() {
        return this.quiz.getCurrentScore();
    }

}
