package com.geoquiz.view.utility;

public enum Difficulty {
    /**
     * Represents the difficulty level "Facile".
     */
    FACILE,
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIO,
    /**
     * Represents the difficulty level "Difficile".
     */
    DIFFICILE;
}
