package com.geoquiz.view.utility;

public enum Modality {
    /**
     * Represents the game mode "Classica".
     */
    CLASSICA,
    /**
     * Represents the game mode "Sfida".
     */
    SFIDA,
    /**
     * Represents the game mode "Allenamento".
     */
    ALLENAMENTO;
}
